import java.util.ArrayList;

public class Building extends Space {

	private String buildingName;
	private ArrayList<Space> childList = new ArrayList<Space>();

	public Building(String name) {
		buildingName = name;
	}

	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public int calculateArea() {
		int area = 0;

		for (Space child : childList) {
			area = area + child.calculateArea();
		}
		return area;
	}
	
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
