import java.util.ArrayList;


public class Sites extends Space {

	private String siteName;
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}
	
	public Sites(String name){
		siteName = name;
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public int calculateArea(){
		int area = 0;
		
		for(Space child: childList){
			area = area + child.calculateArea();
		}
		return area;
	}
	
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
