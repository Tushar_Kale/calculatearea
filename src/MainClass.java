
public class MainClass {

	public static void main(String[] args){
		
		SpaceCreator<Country, State> sc = new SpaceCreator<Country, State>();
		Country country = new Country("USA");
		State mass = new State("Mass");
		State texas = new State("Texas");
		
		sc.addChild(country, mass);
		sc.addChild(country, texas);
		
		SpaceCreator<State, City> sc2 = new SpaceCreator<State, City>();
		City boston = new City("Boston");
		City cambridge = new City("Cambridge");
		City austin = new City("Austin");
		
		sc2.addChild(mass, boston);
		sc2.addChild(mass, cambridge);
		sc2.addChild(mass, austin);
		
		SpaceCreator<State, Sites> sc3 = new SpaceCreator<State, Sites>();
		Sites hustonSite = new Sites("hustonSite");
		
		sc3.addChild(texas, hustonSite);
		
		SpaceCreator<City, Sites> sc4 = new SpaceCreator<City, Sites>();
		Sites camSite1 = new Sites("camSite1");
		Sites camSite2 = new Sites("camSite2");
		
		sc4.addChild(cambridge, camSite1);
		sc4.addChild(cambridge, camSite2);
		
		SpaceCreator<City, Building> sc5 = new SpaceCreator<City, Building>();
		Building bostonBuilding1 = new Building("bostonBuilding1");
		Building bostonBuilding2 = new Building("bostonBuilding2");
		Building austinBuilding = new Building("AustinBuilding");
		
		sc5.addChild(boston, bostonBuilding1);
		sc5.addChild(boston, bostonBuilding2);
		sc5.addChild(austin, austinBuilding);
		
		SpaceCreator<Sites, Building> sc6 = new SpaceCreator<Sites, Building>();
		
		Building camBuilding1 = new Building("camBuilding1");
		Building camBuilding2 = new Building("camBuilding2");
		Building hustonBuilding = new Building("HustonBuilding");
		
		sc6.addChild(camSite1, camBuilding1);
		sc6.addChild(camSite2, camBuilding2);
		sc6.addChild(hustonSite, hustonBuilding);
		
		Floor floor1 = new Floor(1);
		Floor floor2 = new Floor(2);
		Floor floor3 = new Floor(3);
		Floor floor4 = new Floor(4);
		Floor floor5 = new Floor(5);
		Floor floor6 = new Floor(6);
		Floor floor7 = new Floor(7);
		
		SpaceCreator<Building, Floor> sc7 = new SpaceCreator<Building, Floor>();
		sc7.addChild(bostonBuilding1, floor1);
		sc7.addChild(bostonBuilding2, floor2);
		sc7.addChild(camBuilding1, floor3);
		sc7.addChild(camBuilding2, floor4);
		sc7.addChild(austinBuilding, floor5);
		sc7.addChild(austinBuilding, floor6);
		sc7.addChild(hustonBuilding, floor7);
		
		Room room1 = new Room(1);
		room1.setRoomArea(100);
		
		Room room2 = new Room(2);
		room2.setRoomArea(100);
		
		Room room3 = new Room(3);
		room3.setRoomArea(200);
		
		Room room4 = new Room(4);
		
		Room room5 = new Room(5);
		room5.setRoomArea(100);
		
		Room room6 = new Room(6);
		room6.setRoomArea(300);
		
		Room room7 = new Room(7);
		room7.setRoomArea(100);
		
		Room room8 = new Room(8);
		room8.setRoomArea(500);
		
		Room room9 = new Room(9);
		
		SpaceCreator<Floor, Room> sc8 = new SpaceCreator<Floor, Room>();
		sc8.addChild(floor1, room1);
		sc8.addChild(floor2, room2);
		sc8.addChild(floor2, room3);
		sc8.addChild(floor3, room4);
		sc8.addChild(floor4, room5);
		sc8.addChild(floor5, room6);
		sc8.addChild(floor6, room7);
		sc8.addChild(floor7, room8);
		sc8.addChild(floor7, room9);
		
		WorkLab lab1 = new WorkLab(1);
		lab1.setLabArea(50);
		WorkLab lab2 = new WorkLab(2);
		lab2.setLabArea(40);
		WorkLab lab3 = new WorkLab(3);
		lab3.setLabArea(50);
		
		SpaceCreator<Room, WorkLab> sc9 = new SpaceCreator<Room, WorkLab>();
		sc9.addChild(room4, lab1);
		sc9.addChild(room4, lab2);
		sc9.addChild(room9, lab3);
		
		int totalArea = country.calculateArea();
		
		System.out.println("Total Area: " + totalArea);
	}
}
