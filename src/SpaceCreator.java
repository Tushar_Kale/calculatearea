import java.util.Arrays;


public class SpaceCreator<P extends Space,C  extends Space> {

	private String[] hierarchy = {"Country", "State", "City", "Sites", "Building", "Floor", "Room", "WorkLab"};
	
	public void addChild(P parent, C child){
		String  parentName = parent.getClass().getName();
		String childName = child.getClass().getName();
		
		int parentIndex = Arrays.asList(hierarchy).indexOf(parentName);
		int childIndex = Arrays.asList(hierarchy).indexOf(childName);
		
		if(parentIndex < childIndex){
			parent.addChild(child);
		}
	}
}
