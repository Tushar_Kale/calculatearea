import java.util.ArrayList;


public class Country extends Space {

	private String countryName;
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}

	public Country(String name){
		countryName = name;
	}
	
	public String getName() {
		return countryName;
	}

	public int calculateArea(){
		int area = 0;
		
		for(Space child: childList){
			area = area + child.calculateArea();
		}
		return area;
	}

	public void addChild(Space child) {
		childList.add(child);
		
	}
}
