import java.util.ArrayList;


public class Floor extends Space {

	private int floorNumber;
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}
	
	public Floor(int number){
		floorNumber = number;
	}
	
	public int getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}
	
	
	public int calculateArea(){
		int area = 0;
		
		for(Space child: childList){
			area = area + child.calculateArea();
		}
		return area;
	}
	
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
