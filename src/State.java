import java.util.ArrayList;


public class State extends Space {

	private String stateName;
	
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	public State(String name){	
		stateName = name;
	}
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public int calculateArea(){
		int area = 0;
		
		for(Space child: childList){
			area = area + child.calculateArea();
		}
		return area;
	}
	
	@Override
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
