import java.util.ArrayList;


public class Room extends Space {
	
	private int roomNumber;
	private int roomArea = 0;
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}
	
	public Room(int number){
		roomNumber = number;
	}
	
	public int getRoomNumber() {
		return roomNumber;
	}
	
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public int getRoomArea() {
		return roomArea;
	}
	
	public void setRoomArea(int roomArea) {
		this.roomArea = roomArea;
	}
	
	public int calculateArea(){
		int area = 0;
		
		if(roomArea == 0){
			for(Space child: childList){
				area = area + child.calculateArea();
			}
		}
		else{
			area = roomArea;
		}
		
		return area;
	}
	
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
