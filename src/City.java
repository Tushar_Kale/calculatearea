import java.util.ArrayList;


public class City extends Space {

	private String cityName;
	private ArrayList<Space> childList = new ArrayList<Space>();
	
	
	public ArrayList<Space> getChilList() {
		return childList;
	}

	public void setChilList(ArrayList<Space> childList) {
		this.childList = childList;
	}
	public City(String name){
		cityName = name;
	}
	
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public int calculateArea(){
		int area = 0;
		
		for(Space child: childList){
			area = area + child.calculateArea();
		}
		return area;
	}
	
	public void addChild(Space child) {
		childList.add(child);
		
	}
}
