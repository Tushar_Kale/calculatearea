
public class WorkLab extends Space  {
	
	private int labNumber;
	private int labArea;
	
	public WorkLab(int number){
		labNumber = number;
	}
	public int getLabNumber() {
		return labNumber;
	}
	public void setLabNumber(int labNumber) {
		this.labNumber = labNumber;
	}
	public int getLabArea() {
		return labArea;
	}
	
	public void setLabArea(int labArea) {
		this.labArea = labArea;
	}
	
	public int calculateArea(){
		
		return labArea;
	}
	
	public void addChild(Space child) {
		return;
		
	}
}
