
abstract public class Space {

	abstract public int calculateArea();
	
	abstract public void addChild(Space child);
}
