Hi I have implemented the given problem in a generic way.

Now it is not necessary that a country should have a state below it.
It can have anything like state, city, site etc.

So below is the description of the model I have made

At the top most there is a country USA
Its two child entity are two states: Mass and Texas

Mass has two cities as child entities i.e. Boston and Cambridge
while Texas has one city(Austin) and one Site(HustonSite) as child entity
So here we can see that there is no restriction on what would be the child entity.

Similarly the Boston city has two buildings as child entity whereas Cambridge city has two sites as child entity

Austin city has Austin Building as its child entity while Hustonsite has HustonBuilding as its child entity and so on

Also each room will have a defined area for it and if the area for room is 0, then the area of the room would be equal to the sum of area of the worklab in the room.

And if we want that city can be parent entity of state, it can also be implemented.
For this we need to do changes in the class SpaceCreator

In the addChild(P parent, C child) method, we need to do the following changes:

replace the condition:
if(parentIndex < childIndex){
			parent.addChild(child);
		}

by
parent.addChild(child);
i.e. add the child without any if condition

Below is the object model:

![Model.jpg](https://bitbucket.org/repo/6XE8Be/images/3468404187-Model.jpg)